import Vue from "vue";
import App from "./App.vue";
import router from "./router/router.js";
import store from "./store/store.js";
import iview from "iview";
import "iview/dist/styles/iview.css";

Vue.config.productionTip = false;

Vue.use(iview);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");